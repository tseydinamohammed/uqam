package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    void writeOnDisk() {
        MockFileWriter fileWriter = new MockFileWriter();
        Utils.writeOnDisk(fileWriter, "contenue du fichier");
        assertEquals("contenue du fichier", fileWriter.getWrittenData());
    }
}