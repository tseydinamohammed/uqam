package exemple.inf2050;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.io.IOUtils;


public class JSONHash {

    private final String inputFile;
    private final String outputFile;
    private JSONObject jsonObj;

    public JSONHash(String inputFile) {
        this.jsonObj = null;
        this.inputFile = inputFile;
        this.outputFile = null;
    }

    public boolean load() throws JSONException, LabINF2050Exception{
        boolean result;
        try {
            String stringJson = IOUtils.toString(new FileInputStream(this.inputFile),  "UTF-8");
            JSONObject jsonObj = (JSONObject) JSONSerializer.toJSON(stringJson);
            this.jsonObj = jsonObj;
            result = true;
        }catch(FileNotFoundException e){
            throw new LabINF2050Exception("Le fichier d'entré n'exixte pas");
        }catch(JSONException e){
            throw new LabINF2050Exception("Le fichier json n'est pas valide");
        }catch(IOException e){
            throw new LabINF2050Exception(e.toString());
        }
        return result;
    }

    public int countAlbum(){
        int cpt_album = 0;
        JSONArray collection = (JSONArray) JSONSerializer.toJSON(jsonObj.getString("collection"));
        for(int i = 0 ; i < collection.size(); i++){
            if (collection.getJSONObject(i).getString("type").equals("album")){
                cpt_album += 1;
            }
        }
        return cpt_album;
    }

    public int countSingle(){
        int cpt_single = 0;
        JSONArray collection = (JSONArray) JSONSerializer.toJSON(jsonObj.getString("collection"));
        for(int i = 0 ; i < collection.size(); i++){
            if (collection.getJSONObject(i).getString("type").equals("single")){
                cpt_single += 1;
            }
        }
        return cpt_single;
    }

    public ArrayList<JSONObject> getListAlbumFrom2003(){

        ArrayList<JSONObject> liste = new ArrayList<>();

        JSONArray collection = (JSONArray) JSONSerializer.toJSON(jsonObj.getString("collection"));
        for(int i = 0 ; i < collection.size(); i++){
            if (collection.getJSONObject(i).getInt("publication") >= 2003){
                liste.add(collection.getJSONObject(i));
            }
        }
        return liste;

    }

    public void showAll(ArrayList<JSONObject> liste){
        liste.forEach((elem) -> {
            System.out.println(elem.toString(3));
        });
    }

    public ArrayList<JSONObject> getListAlbumNote5(){

        JSONObject msgObj = new JSONObject();
        ArrayList<JSONObject> liste = new ArrayList<>();

        JSONArray collection = (JSONArray) JSONSerializer.toJSON(jsonObj.getString("collection"));
        for(int i = 0 ; i < collection.size(); i++){
            if (collection.getJSONObject(i).getInt("rating") == 5){
                liste.add(collection.getJSONObject(i));
            }
        }
        return liste;
    }

    public JSONObject getListAlbumPreferes() throws LabINF2050Exception {

        ArrayList<String> listePrefere = new ArrayList<>();
        listePrefere.add("Century Child");
        listePrefere.add("The System has Failed");
        listePrefere.add("Land of the Free");

        ArrayList<JSONObject> liste = new ArrayList<>();

        JSONArray collection = (JSONArray) JSONSerializer.toJSON(jsonObj.getString("collection"));
        for(int i = 0 ; i < collection.size(); i++){

            if (collection.getJSONObject(i).getString("title").equals("Century Child")
                    || collection.getJSONObject(i).getString("title").equals("The System has Failed")
                    || collection.getJSONObject(i).getString("title").equals("Land of the Free")){

                liste.add(collection.getJSONObject(i));
            }
        }
        return createJsonObj(liste);
    }

    private JSONObject createJsonObj(ArrayList<JSONObject> liste) {

        JSONObject newJsonObj = new JSONObject();
        newJsonObj.put("preferes", liste);

        return newJsonObj;
    }

    public boolean save() throws LabINF2050Exception {
        boolean res;
        try (FileWriter f = new FileWriter(outputFile)) {
            f.write(jsonObj.toString(3));
            f.flush();
            f.close();
            res = true;
        }catch(IOException e){
            throw new LabINF2050Exception(e.toString());
        }
        return res;
    }

}
