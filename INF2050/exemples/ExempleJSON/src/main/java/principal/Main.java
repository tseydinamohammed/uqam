package principal;

import domain.JSONExemple;

public class Main {

    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.out.println("Invalid arguments");
            System.exit(1);
        }

        JSONExemple json = new JSONExemple(args[0], args[1]);
        json.load();
        json.to_string();
        json.save();
    }
}
