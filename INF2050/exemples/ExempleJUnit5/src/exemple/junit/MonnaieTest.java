package exemple.junit;

import org.junit.jupiter.api.*;
import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.*;


class MonnaieTest {

    Monnaie un ;
    Monnaie deux;
    Monnaie trois;
    Monnaie six;


    @BeforeEach
    void setUp() {
        // Exécutée avant chaque test.
        un = new Monnaie("1");
        deux = new Monnaie("2");
        trois = new Monnaie("3");
        six = new Monnaie("6");
    }

    @AfterEach
    void tearDown() {
        // Exécutée après chaque test
        un = null;
        deux = null;
        trois = null;
        six = null;
    }

    @Test
    void testToString() {
        String expected = "1$";
        String actual = un.toString();
        assertEquals(expected, actual);
    }

    @Test
    void testToString1() {
        String expected = "2$";
        String actual = un.toString();
        assertNotEquals(expected, actual);
    }

    @Test
    @DisplayName("Additionne deux nombres")
    void additionner() {

        assertEquals("3$", un.additionner(deux));
    }

    @Test
    void soustraction_valide() throws OperationInvalideException {
        assertEquals("1$", trois.soustraire(deux));
    }

    @Test
    void soustraction_invalide()  {
        assertThrows(OperationInvalideException.class, () -> {
            deux.soustraire(trois);
        });
    }

    @Test
    void multiplier() {
        assertEquals("6$", deux.multiplier(trois));
    }

    @Test
    @DisplayName("Multiplie 2 nombres en moins de 3 secondes")
    void multiplication_rapide() {
        assertTimeout(ofSeconds(3),
                () -> {six.multiplier(trois);
        });
    }

    @Test
    @DisplayName("Rejette les strings qui ne sont pas nombres")
    void interceptStrings() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Monnaie("INF2050");
        });
    }

    @Test
    @DisplayName("Accepte les strings qui représentent des nombres")
    void interceptNombres() {
        assertAll("nombres entiers",
                () -> assertNotNull(new Monnaie("6")),
                () -> assertNotNull(new Monnaie("20")),
                () -> assertNotNull(new Monnaie("10"))
        );
    }

    @Test
    @Disabled("N'est pas encore implémenté")
    void pourcent() {
        // desactivé
    }
}