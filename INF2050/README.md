# UQAM - INF2050

Cours d'utilisation d'outils et de pratique de développement logiciel dans un environnement professionnel. Axé sur la gestion de source, les tests unitaires (JUnit) et les outils de builds.

## Contenu du cours

- Comparaison des modèles de développement traditionnels et des processus de développements modernes.
- Développement logiciel dans un contexte de logiciel libre (`open source`).
- Étude de cas
- Compréhension de tests (JUNIT)
- Gestion de la configuration
- Construction automatisée (Maven)
- Environnement intégré de développement
- Outils de pistage et de revue de code

## Accompagement sur TP

* Répondre aux questions sans donner la solution du TP
* Orienter l'étudiant(e)s dans sa démarche. 

## Licence

Apache 2.0 : Vous êtes libre d'utiliser, modifier et distribuer ce code, tant que vous citez l'auteur original.

## Auteur

Serge Dogny


