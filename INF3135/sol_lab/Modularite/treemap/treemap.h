#ifndef TREEMAP_H
#define TREEMAP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct tree_node {
    char *key;               // Key
    char *value;             // Value
    struct tree_node *left;  // Left subtree
    struct tree_node *right; // Right subtree
};

typedef struct {
    struct tree_node *root; // Root of tree
} treemap;

/**
 * Initialize an empty tree map
 *
 * @param t  The tree map to initialize
 */
void treemap_initialize(treemap *t);

/**
 * Return the value associated with the given key in a tree map
 *
 * Note: if the key does not exist, return NULL.
 *
 * @param t    The tree map
 * @param key  The key to search
 */
char *treemap_get(const treemap *t, const char *key);

/**
 * Set the value for the given key in a tree map
 *
 * @param t      The tree map
 * @param key    The key
 * @param value  The value
 */
void treemap_set(treemap *t, const char *key, const char *value);

/**
 * Indicate if a key exists in a tree map
 *
 * @param t    The tree map
 * @param key  The key
 * @return     True if and only if the key exists in the tree map
 */
bool treemap_has_key(const treemap *t, const char *key);

/**
 * Print a tree map to stdout
 *
 * @param t       The tree map to print
 * @param prefix  The prefix to print for each line
 */
void treemap_print(const treemap *t,
                   const char *prefix);

/**
 * Delete a tree map
 *
 * @param t  The tree map to delete
 */
void treemap_delete(treemap *t);

#endif
