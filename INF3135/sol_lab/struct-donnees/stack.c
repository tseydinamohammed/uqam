#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Types
// -----

struct stack_node {
    char value;              // Value in node
    struct stack_node *next; // Next node
};

typedef struct {
    struct stack_node *first; // Pointer to first node
    unsigned int size;        // Number of objets in the stack
} stack;

// Interface
// ---------

/**
 * Initializes a stack
 *
 * Note: do not forget to call `stack_delete` when the stack is not needed
 * anymore.
 *
 * @return  The stack
 */
void stack_initialize(stack *s);

/**
 * Returns true if the stack is empty
 *
 * @param s  The stack to check
 * @return   true if and only if the stack is empty
 */
bool stack_is_empty(const stack *s);

/**
 * Pushes an element on top of the stack
 *
 * @param s      The stack
 * @param value  The element to push
 */
void stack_push(stack *s, char value);

/**
 * Pops the element at the top of a stack
 *
 * The element is removed from the stack and returned.
 *
 * @param s  The stack
 * @return   The value of the top of the stack
 */
char stack_pop(stack *s);

/**
 * Deletes a stack
 *
 * @param s  The stack to delete
 */
void stack_delete(stack *s);

// Implementation
// --------------

void stack_initialize(stack *s) {
    s->first = NULL;
    s->size = 0;
}

bool stack_is_empty(const stack *s) {
    return s->size == 0;
}

void stack_push(stack *s, char value) {
    struct stack_node *node = malloc(sizeof(struct stack_node));
    node->value = value;
    node->next = s->first;
    s->first = node;
    ++s->size;
}

char stack_pop(stack *s) {
    if (!stack_is_empty(s)) {
        char value = s->first->value;
        struct stack_node *node = s->first;
        s->first = node->next;
        free(node);
        --s->size;
        return value;
    } else {
        fprintf(stderr, "Cannot pop from empty stack\n");
        exit(1);
        return '?';
    }
}

void stack_delete(stack *s) {
    while (!stack_is_empty(s)) stack_pop(s);
}

// Testing
// -------

/**
 * Returns true if and only if an expression is balanced
 *
 * An expression is balanced if each opening parenthesis, bracket or brace is
 * matched with its corresponding closing symbol.
 *
 * @param expr  The expression to check
 * @returns     True if and only if the expression is balanced
 */
bool is_balanced(char *expr) {
    bool balanced = true;
    stack s;
    stack_initialize(&s);
    for (unsigned int i = 0; balanced && expr[i] != '\0'; ++i) {
        if (expr[i] == '(') {
            stack_push(&s, ')');
        } else if (expr[i] == '[') {
            stack_push(&s, ']');
        } else if (expr[i] == '{') {
            stack_push(&s, '}');
        } else if (expr[i] == ')' || expr[i] == ']' || expr[i] == '}') {
            if (stack_is_empty(&s))
                balanced = false;
            else
                balanced = expr[i] == stack_pop(&s);
        }
    }
    balanced = balanced && stack_is_empty(&s);
    stack_delete(&s);
    return balanced;
}

int main(void) {
    char expr1[] = "[x * (y + 2)] / {3 - [z + (2 - x)]}";
    char expr2[] = "[x * (y + 2)] / 3 - [z + (2 - x)]}";
    char expr3[] = "{[}]";
    char expr4[] = "[[{}()]()]";
    char expr5[] = "([";
    char expr6[] = "";

    printf("Is the expression \"%s\" balanced ? %s\n", expr1,
           is_balanced(expr1) ? "yes" : "no");
    printf("Is the expression \"%s\" balanced ? %s\n", expr2,
           is_balanced(expr2) ? "yes" : "no");
    printf("Is the expression \"%s\" balanced ? %s\n", expr3,
           is_balanced(expr3) ? "yes" : "no");
    printf("Is the expression \"%s\" balanced ? %s\n", expr4,
           is_balanced(expr4) ? "yes" : "no");
    printf("Is the expression \"%s\" balanced ? %s\n", expr5,
           is_balanced(expr5) ? "yes" : "no");
    printf("Is the expression \"%s\" balanced ? %s\n", expr6,
           is_balanced(expr6) ? "yes" : "no");
}
