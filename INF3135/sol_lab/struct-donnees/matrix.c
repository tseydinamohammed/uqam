#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

// Type
// ----

/**
 * A 2D matrix of doubles
 */
struct matrix {
    unsigned int r;  // Number of rows
    unsigned int c;  // Number of columns
    double **values; // Values in matrix
};

// Interface
// ---------

/**
 * Initialize a matrix
 *
 * @param m       The matrix to initialize
 * @param r       The number of rows
 * @param c       The number of columns
 * @param random  If true, the values are selected randomly
 *                If false, the values are 0.0
 */
void matrix_initialize(struct matrix *m,
                       unsigned int r,
                       unsigned int c,
                       bool random);

/**
 * Print a matrix to stdout
 *
 * @param m  The matrix to print
 */
void matrix_print(const struct matrix *m);

/**
 * Add the matrix `m2` to the matrix `m1`
 *
 * @param m1  The matrix that is added
 * @param m2  The second matrix
 */
void matrix_add(struct matrix *m1, const struct matrix *m2);

/**
 * Delete the given matrix
 *
 * @param m  The matrix to delete
 */
void matrix_delete(struct matrix *m);

// Implementation
// --------------

void matrix_initialize(struct matrix *m,
                       unsigned int r,
                       unsigned int c,
                       bool random) {
    m->r = r;
    m->c = c;
    m->values = malloc(r * sizeof(double*));
    for (unsigned int i = 0; i < r; ++i) {
        m->values[i] = malloc(c * sizeof(double));
        for (unsigned int j = 0; j < c; ++j) {
            if (random) {
                m->values[i][j] = (float)rand() / (float)(RAND_MAX / 20.0) - 10.0;
            } else {
                m->values[i][j] = 0.0;
            }
        }
    }
}

void matrix_print(const struct matrix *m) {
    for (unsigned int i = 0; i < m->r; ++i) {
        printf("[ ");
        for (unsigned int j = 0; j < m->c; ++j) {
            printf("%6.2lf ", m->values[i][j]);
        }
        printf("]\n");
    }
}

void matrix_add(struct matrix *m1, const struct matrix *m2) {
    if (m1->r != m2->r || m1->c != m2->c) {
        fprintf(stderr, "Error: matrices have different dimensions\n");
        exit(1);
    }
    for (unsigned int i = 0; i < m1->r; ++i)
        for (unsigned int j = 0; j < m1->c; ++j)
            m1->values[i][j] += m2->values[i][j];

}

void matrix_delete(struct matrix *m) {
    for (unsigned int i = 0; i < m->r; ++i)
        free(m->values[i]);
    free(m->values);
}

// Testing
// -------

int main(void) {
    srand(time(NULL));
    struct matrix m1, m2;
    printf("Initializing m1:\n");
    matrix_initialize(&m1, 3, 5, true);
    matrix_print(&m1);
    printf("Initializing m2:\n");
    matrix_initialize(&m2, 3, 5, true);
    matrix_print(&m2);
    printf("Adding m2 to m1:\n");
    matrix_add(&m1, &m2);
    matrix_print(&m1);
    matrix_delete(&m1);
    matrix_delete(&m2);
    return 0;
}
