---
wip: true
sigle: INF3135
title: Construction et maintenance de logiciels
session: Été 2021
encart-aide: true
resp:
  nom: Beaudry
  prenom: Éric
  local: PK-4635
  tel: poste 4005
  matricule: BEAE02
  job: Professeur
  email: beaudry.eric@uqam.ca
  web: http://ericbeaudry.ca/
ens:
- matricule: DOGG0
  nom: Dogny
  prenom: Gnagnely Serge
  groupes:
  - '020'
---


# Description du cours

## Objectif général du cours

Initier les étudiants à la programmation à l'aide d'un langage impératif et procédural. 
Familiariser les étudiants à la construction professionnelle de logiciels et à leur maintenance.


## Sommaire du contenu

* Notions de base de la programmation procédurale et impérative en langage
  C sous environnement Unix/Linux (définition et déclaration, portée et durée
  de vie, fichier d'interface, structures de contrôle, unités de programme et
  passage des paramètres, macros, compilation conditionnelle).
* Décomposition en modules et caractéristiques facilitant les modifications
  (cohésion et couplage, encapsulation et dissimulation de l'information,
  décomposition fonctionnelle).
* Style de programmation (conventions, documentation interne, gabarits).
* Débogage de programmes (erreurs typiques, traces, outils).
* Assertions et conception par contrats.
* Tests (unitaires, intégration, d'acceptation, boîte noire vs. boîte blanche,
  mesures de couverture, outils d'exécution automatique des tests).
* Évaluation et amélioration des performances (profils d'exécution,
  améliorations asymptotiques vs. optimisations, outils).
* Techniques et outils de base pour la gestion de la configuration. Système de
  contrôle de version.


## Modalité d'enseignement

Ce cours comporte une séance obligatoire de laboratoire (2 heures).


## Préalables académiques

* INF1120 - Programmation I (pour le certificat en réseaux et systèmes de
  télécommunications et le baccalauréat en systèmes informatiques et électroniques)
* INF2050 - Outils et pratiques de développement logiciel


# Objectif détaillé du cours

Ce cours vise à introduire les étudiants à la construction professionnelle de
logiciels. Il vise aussi à familiariser les étudiants avec la programmation
procédurale et impérative.

À la fin du cours, l'étudiant devrait être capable de :

- développer et modifier des composants logiciels écrits dans un langage
  impératif et procédural;
- bien maîtriser le langage C et le compilateur du C sous UNIX/Linux;
- utiliser les notions de module, de cohésion et couplage, de complexité
  structurale, de dissimulation de l'information, etc., pour évaluer la qualité
  d'un composant logiciel;
- expliquer et utiliser les principales techniques de modularisation :
  décomposition fonctionnelle, dissimulation de l'information, filtres et
  pipelines;
- utiliser des assertions (pré/post-conditions, invariants) pour documenter
  des composants logiciels et assurer leur bon fonctionnement;
- déboguer un programme à l'aide de techniques, stratégies et outils
  appropriés;
- vérifier le bon fonctionnement d'un composant logiciel à l'aide de tests
  fonctionnels et structurels, et d'évaluer à l'aide des notions et outils
  appropriés la qualité des tests (par ex., complexité cyclomatique, mesures de
  couvertures des tests);
- évaluer de façon empirique à l'aide d'outils appropriés (par ex., profils
  d'exécution) les performances d'un composant logiciel de façon à pouvoir, si
  nécessaire, en améliorer les performances;
- utiliser divers outils (outil de gestion de configuration, fichiers
  Makefile, langage de scripts) pour organiser le développement de programmes
  comportant plusieurs composants ou modules;
- expliquer les notions de base de la maintenance des logiciels et
  appliquer certaines techniques de maintenance (tests de régression exécutés
  automatiquement, remodelage de programmes).


# Contenu du cours

**Environnement Linux, introcution au langage C et outils de développement.** Environnement \*NIX (shell et ligne de commande),
introduction au langage C (Hello World),
Phases de compilation,
Éditeurs de texte,
Environnements de développement,
Gestion de sources (rappel).

**Les bases du langage C.**
Types de bases,
Variables et constantes,
Opérateurs,
Mots-clés,
Instructions de contrôle d'exécution,
Structures et unions,
Variables et leur portée,
Énoncés et expressions,
Fichiers,
Fonctions,
Pointeurs,
Tableaux,
Tableaux à plusieurs dimensions,
Préprocesseur,
Mémoirs et allocation dynamique,
Entrées et sorties(printf, scanf, stdin, stdout, stderr),
Tubes et redirection.

**Construction automatisée de base.**
Makefile.

**Autres outils de compilation et configuration automatique.**
Limite de make, aperçu d'outils typiques (CMake, Autoconf/make)

**Modularité.**
Cohésion et couplage.
Encapsulation et dissimulation de l'information,
Décomposition fonctionnelle.

**Maintenance.** 
Notion de base de la maintenant de logiciels, Types de maintenance.

**Tests.**
Niveaux de tests (boîte noire, boîte blanche)
Types de tests (Unitaires, Intégration, Acceptation).

**Déboggage de programmes.**
Erreurs typiques.
Outils : gdb, valgrind; techniques.

**Structures de données.**
Tableaux dynamique,
Ensemble,
Tableau associatif,
Piles,
Files.

**Évaluation et amélioration des performances.**
Profils d'exécution, améliorations asymptotiques vs. optimisations.
Outils.

**Bibliothèques en C.**
Conception et intégration; statique vs dynamique,
Aperçu de bibliothèques populaires.


# Calendrier


| #Semaine | Cours |
| :---:  | :---    |
 1 | Environnement Linux et introduction au C
 2 | Base du langage C - partie 1
 3 | Base du langage C - partie 2
 4 | Base du langage C - partie 3
 5 | Modularité
 6 | Construction automatisée 
 **7**  | **_Revision Examen Intra_**
 8 | Autres outils de compilation et configuration automatique  
 9 | Maintenance
 10  |  Tests
 11 | Débogage de programmes
 12 | Structures de données
 13 |  Évaluation et amélioration des performances
 14 | Bibliothèques en C 
 **15**  | **_Revision Examen final_**


# Ateliers (laboratoires)

+ Les ateliers concernent la matière vue.
+ Ne pas prendre de retard dans vos devoirs.
+ Les ateliers sont obligatoires et individuels à raison de 2h par semaine.
+ Les ateliers sont évalués.


# Modalités d'évaluation

  | Description sommaire | Date                         | Pondération |
  |----------------------|:-----------------------------|:-----------:|
  | Examen intra         | 25 - 27 juin 2021                  |    25 %     |
  | Examen final         | 18 - 20 août 2021                   |    25 %     |
  | TP1                  | Remise : 23 mai 2021 à 23:55                 |    10 %     |
  | TP2                  | Remise : 27 juin 2021 à 23:55                |    15 %     |
  | TP3                  | Remise : 11 juillet 2021 à 23:55                |    10 %     |
  | TP4                  | Remise : 15 août 2021 à 23:55                |    15 %     |
  
  L'étudiant doit obtenir 50%+ dans les examens et 50%+ dans les travaux afin
  de réussir le cours. Dans le cas contraire, l'échec sera attribué.
  
  Les règlements concernant le plagiat seront strictement appliqués.
  Pour plus de renseignements, consultez le site suivant :
  <http://www.infosphere.uqam.ca/rediger-un-travail/eviter-plagiat>


# Entente d'évaluation

 - L'entente d'évaluation est un document officiel qui contient les modalités d'évaluations retenues.


## Directives aux examens

 + Avoir votre carte d'étudiant UQAM valide.
 + L'examen à faire seul sans aucune aide externe.
 + Vous ne pouvez pas collaborer avec un partenaire.
 + Vous ne pouvez pas prendre les notes personnelles d'un voisin.
 + Bien lire les questions.
 + Vous devez réfléchir avant de répondre.
 + Répondre dans les espaces prévus.
 + WebCamera et micro actif durant toute la durée de l'examen.
 + **Signer la feuille de présence avant votre départ, si applicable.**


## Directives des travaux pratiques

 + Remise électronique **privée** via le GitLab du département d'informatique.
 + Travaux à faire individuellement.
 + La qualité du français sera prise en considération (jusqu’à 10% de pénalité).


## Communication

 + Vous devez **toujours** et uniquement utiliser votre courriel UQAM lors de vos communications avec l'enseignant.
 + ListServ est utilisé pour les communications aux groupes.
 + ListServ UQAM fonctionne uniquement avec le courriel UQAM.


## Conclusion

Le langage C est intimement lié au système d'exploitation Unix (et vice versa).  Aujourd'hui, nous 
parlons de Linux un `OS`, en anglais, qui est stable et performant.  `Linux` et `Bash` seront vus
et sont nécessaires pour l'accomplissement de tâches dans le cadre de ce cours.

Le matériel sera mis à jour sur une base régulière.


# Médiagraphie


VC Kernighan, B.W. et Ritchie, D.M. -- Le langage C -- deuxième édition, MASSON, PARIS, 1990.

UC <https://info.uqam.ca/~privat/INF1070/>. Site web du cours INF1070
Utilisation et administration des systèmes informatiques.

VC Blaquelaire, J.P. -- _Méthodologie de la programmation en C (4e édition)_ --
DUNOD, 2005. [QA76.73 C15B75].

VC Kernighan, B.W. et R. Pike, R. -- _The Practice of Programming_ --
ADDISON-WESLEY, 1999.

VC Kernighan, B.W. et R. Pike, R. -- _La programmation - En pratique_ --
VUIBERT, 2001. [QA76.6K48814].

VC Loukides, M. et Oram, A. -- _Programming with GNU Software_ -- O'REILLY,
1997.

VC McConnell, S. -- _Code Complete - A Practical Handbook of Software
Construction_ -- (SECOND EDITION). MICROSOFT PRESS, REDMOND, WA, 2004.

VC ZELLER, A. and KRINKE, J. -- _Essential Open Source Toolset_ -- JOHN WILEY
& SONS, LTD, 2005.

UR <http://www.info2.uqam.ca/~tremblay/INF3135>. Site web contenant des notes
de cours et transparents, des anciens examens, etc.
