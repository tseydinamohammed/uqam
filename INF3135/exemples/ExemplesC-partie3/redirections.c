#include <stdio.h>

int main(void) {
    char c = getchar();
    if (c == 'y')
        printf("Yes!\n");
    else
        fprintf(stderr, "No!\n");
    return 0;
}
