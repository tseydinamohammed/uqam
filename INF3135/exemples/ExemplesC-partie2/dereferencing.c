/*
 * menu.c
 * Author : Serge D
 */

#include <stdio.h>

int main(void) {
    double d = 1.5, *p = &d;
    printf("d     = %lf\n", d);
    printf("&d    = %p\n" , &d);
    printf("p     = %p\n" , p);
    printf("*p    = %lf\n", *p);
    printf("*(&d) = %lf\n", *(&d));
    printf("&(*p) = %p\n" , &(*p));
    return 0;
}
