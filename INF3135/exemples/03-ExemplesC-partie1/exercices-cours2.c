/*
 * exercices-cours2
 * Author : Serge D
 */
#include <stdio.h>

void print_n(char c, unsigned int n);
void print_triangle(char c, unsigned int n);
void print_checkerboard(char c, unsigned int m, unsigned int n);

void print_n(char c, unsigned int n) {
    for (unsigned int i = 0; i < n; i++)
        printf("%c", c);
}

void print_triangle(char c, unsigned int n) {
    for (unsigned int m = n; m > 0; m--) {
        print_n(c, m);
        printf("\n");
    }
}

void print_checkerboard(char c, unsigned int m, unsigned int n) {
    for (unsigned int i = 0; i < m; i++) {
        for (unsigned int j = 0; j < n; j++) {
            printf("%c", (i + j) % 2 == 0 ? c : ' ');
        }
        printf("\n");
    }
}

int main(void) {
    //print_n('*', 5);
    //print_triangle('.', 5);
    print_checkerboard('*', 8, 5);
    printf("\n");
    return 0;
}
