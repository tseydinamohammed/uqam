/*
 * hilopriv.h
 * Author : Serge D
 */
#ifndef HILOPRIV_H
#define HILOPRIV_H

enum Bool {FAUX, VRAI};

int obtenirNombreAleatoire();
int saisirNombre(int);

#endif
