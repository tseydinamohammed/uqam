# Labo 4: Tableaux et pointeurs

Dans ce laboratoire, vous allez télécharger des données géographiques
disponibles sur le site [GeoNames](https://www.geonames.org/), puis écrire un
programme en C qui permet de les manipuler.

## 1 - Téléchargement des données (10 minutes)

Dans un premier temps, vous allez récupérer les données:

* À l'aide de la commande `curl` ou `wget`, téléchargez le fichier situé
  à l'URL
  [http://www.geonames.org/export/zip/CA.zip](http://www.geonames.org/export/zip/CA.zip)
* Puis, décompressez l'archive à l'aide de la commande `unzip`
* Vérifiez rapidement le contenu des fichiers `CA.txt` et `readme.txt`

## 2 - Nettoyage des données (20 minutes)

Il est maintenant temps d'extraire les informations pertinentes:

* En consultant le fichier `readme.txt`, identifiez les numéros de colonne qui
  correspondent

    1. au code postal (*postal code*)
    2. au nom du lieu (*place name*)
    3. à la province ou le territoire (*admin code1*)
    4. à la latitude
    5. à la longitude.

* À l'aide de la commande `cut`, transformez le fichier pour ne conserver que
  les informations mentionnées à l'item précédent, en les séparant par des
  virgules plutôt que des tabulations. Redirigez le résultat dans un fichier
  nommé `clean-CA.txt`. Les options `-f` et `--output-delimiter` de la commande
  `cut` vous seront utiles. À la fin, vous devriez obtenir quelque chose qui
  ressemble à ceci:

```sh
$ head -n 5 clean-CA.txt
T0A,Eastern Alberta (St. Paul),AB,54.766,-111.7174
T0B,Wainwright Region (Tofield),AB,53.0727,-111.5816
T0C,Central Alberta (Stettler),AB,52.4922,-112.8113
T0E,Western Alberta (Jasper),AB,53.4021,-117.2308
T0G,North Central Alberta (Slave Lake),AB,55.6993,-114.4529
```

* (optionnel) Cherchez les trois premières lettres de votre code postal dans le
  fichier `clean-CA.txt` pour confirmer qu'il correspond bien à votre lieu de
  domicile.

## 3 - Écriture des fonctions de base (30 minutes)

Maintenant que les données sont prêtes et nettoyées, vous allez commencer
à organiser votre programme C pour les manipuler.

* Ouvrez le fichier `place.c` et consultez son contenu. Lisez bien les
  déclarations des types, ainsi que les signatures des fonctions que vous
  devrez compléter.
* Complétez la fonction `load_list` qui charge une liste de lieux géographiques
  en lisant sur `stdin`. Les fonctions `fgets`, `strtok`, `strncpy` et `strtof`
  vous seront utiles. Profitez de l'arithmétique des pointeurs pour parcourir
  la liste sans utiliser la notation `[]`.
* Complétez la fonction `print_list` qui affiche une liste de lieux
  géographiques sur `stdout`. Ici aussi, essayez d'éviter la notation `[]` et
  d'utiliser seulement l'arithmétique des pointeurs.
* À la fin, vous devriez obtenir quelque chose qui ressemble à ceci:

```sh
$ gcc place.c -o place
$ head -n 10 clean-CA.txt | ./place load-and-print
Place{name=Eastern Alberta (St. Paul),admin_code=AB,postal_code=T0A,latitude=54.765999,longitude=-111.717400}
Place{name=Wainwright Region (Tofield),admin_code=AB,postal_code=T0B,latitude=53.072701,longitude=-111.581596}
Place{name=Central Alberta (Stettler),admin_code=AB,postal_code=T0C,latitude=52.492199,longitude=-112.811302}
Place{name=Western Alberta (Jasper),admin_code=AB,postal_code=T0E,latitude=53.402100,longitude=-117.230797}
Place{name=North Central Alberta (Slave Lake),admin_code=AB,postal_code=T0G,latitude=55.699299,longitude=-114.452904}
Place{name=Northwestern Alberta (High Level),admin_code=AB,postal_code=T0H,latitude=57.540298,longitude=-116.915298}
Place{name=Southeastern Alberta (Drumheller),admin_code=AB,postal_code=T0J,latitude=50.994400,longitude=-111.463203}
Place{name=International Border Region (Cardston),admin_code=AB,postal_code=T0K,latitude=49.472099,longitude=-112.240799}
Place{name=Kananaskis Country (Claresholm),admin_code=AB,postal_code=T0L,latitude=50.631401,longitude=-114.408897}
Place{name=Central Foothills (Sundre),admin_code=AB,postal_code=T0M,latitude=51.955200,longitude=-114.869102}
```

## 4 - Trier les lieux (30 minutes)

Vous allez maintenant ajouter des fonctions qui permettent d'ordonner les lieux
selon différentes relations d'ordre.

* Complétez la fonction `sort_by_postal_code` de telle sorte que les lieux
  géographiques soient ordonnés selon le code postal. À la fin, vous devriez
  obtenir quelque chose qui ressemble à ceci:

```sh
$ gcc place.c -o place
$ ./place by-postal-code < clean-CA.txt | head -n 10
Place{name=Southeastern Avalon Peninsula (Ferryland),admin_code=NL,postal_code=A0A,latitude=47.219501,longitude=-53.144798}
Place{name=Western Avalon Peninsula (Argentia),admin_code=NL,postal_code=A0B,latitude=47.327000,longitude=-53.660900}
Place{name=Bonavista Peninsula (Bonavista),admin_code=NL,postal_code=A0C,latitude=48.384300,longitude=-53.908100}
Place{name=Burin Peninsula (Marystown),admin_code=NL,postal_code=A0E,latitude=47.560699,longitude=-54.780102}
Place{name=Northeast Newfoundland (Lewisporte),admin_code=NL,postal_code=A0G,latitude=48.865700,longitude=-54.729599}
Place{name=Central Newfoundland (Bishops Falls),admin_code=NL,postal_code=A0H,latitude=48.431099,longitude=-56.175800}
Place{name=Northern Newfoundland (Springdale),admin_code=NL,postal_code=A0J,latitude=49.487499,longitude=-56.080799}
Place{name=Northwest Newfoundland/Eastern Labrador (Mary's Harbour),admin_code=NL,postal_code=A0K,latitude=52.056301,longitude=-57.217499}
Place{name=Western Newfoundland (Lark Harbour),admin_code=NL,postal_code=A0L,latitude=48.944500,longitude=-58.064499}
Place{name=Southwestern Newfoundland (Channel-Port aux Basques),admin_code=NL,postal_code=A0M,latitude=47.803699,longitude=-58.386902}
```

* Complétez la fonction `sort_by_latitude` de telle sorte que les lieux
  géographiques soient ordonnés selon leur latitude.
* Quelle commande permet d'afficher les 10 lieux les plus au nord?

## 5 - Calcul de statistiques (30 minutes)

Pour terminer, vous allez calculer des statistiques simples sur les différents
lieux géométriques:

* L'*étendue de latitude* (en anglais, *latitude scope*) est la différence
  maximale qui existe entre les latitudes de deux lieux géométriques
* L'*étendue de longitude* (en anglais, *longitude scope*) est la différence
  maximale qui existe entre les longitudes de deux lieux géométriques
* Le *diamètre* (en anglais *diameter*) est la distance maximale qui existe
  entre deux lieux géométriques
* Complétez la fonction `print_statistics` afin qu'elle affiche l'étendue de
  latitude, l'étendue de longitude et le diamètre de la liste de lieux
  géographiques enregistrés dans le fichier `clean-CA.txt`
