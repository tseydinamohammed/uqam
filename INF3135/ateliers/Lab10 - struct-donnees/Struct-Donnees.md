# Labo 9: Structures de données

## Implémentation d'une file

Voici l'interface d'une structure une structure de données de type "file" (en
anglais *queue*). Proposez une implémentation de cette interface :

```c
// Types
// -----

struct queue_node {
    unsigned int value;      // Value in node
    struct queue_node *prev; // Previous node
    struct queue_node *next; // Next node
};

typedef struct {
    struct queue_node *first; // Pointer to first node
    struct queue_node *last;  // Pointer to last node
} queue;

// Interface
// ---------

/**
 * Initializes a queue
 *
 * Note: do not forget to call `queue_delete` when the queue is not needed
 * anymore.
 *
 * @return  The queue
 */
void queue_initialize(queue *q);

/**
 * Returns true if the queue is empty
 *
 * @param q  The queue to check
 * @return   true if and only if the queue is empty
 */
bool queue_is_empty(const queue *q);

/**
 * Pushes an element at the end of the queue
 *
 * @param q      The queue
 * @param value  The element to push
 */
void queue_push(queue *q, unsigned int value);

/**
 * Pops the first element of a queue
 *
 * The element is removed from the queue and returned.
 *
 * @param q  The queue
 * @return   The value of the top of the queue
 */
char queue_pop(queue *q);

/**
 * Prints the queue to stdout
 *
 * @param q  The queue
 */
void queue_print(const queue *q);

/**
 * Deletes a queue
 *
 * @param q  The queue to delete
 */
void queue_delete(queue *q);
```

## Implémentation d'une matrice

Proposez une l'implémentation d'une  matrice (*matrix*) à partir de l'interface suivante :

```c
// Type
// ----

/**
 * A 2D matrix of doubles
 */
struct matrix {
    unsigned int r;  // Number of rows
    unsigned int c;  // Number of columns
    double **values; // Values in matrix
};

// Interface
// ---------

/**
 * Initialize a matrix
 *
 * @param m       The matrix to initialize
 * @param r       The number of rows
 * @param c       The number of columns
 * @param random  If true, the values are selected randomly
 *                If false, the values are 0.0
 */
void matrix_initialize(struct matrix *m,
                       unsigned int r,
                       unsigned int c,
                       bool random);

/**
 * Print a matrix to stdout
 *
 * @param m  The matrix to print
 */
void matrix_print(const struct matrix *m);

/**
 * Add the matrix `m2` to the matrix `m1`
 *
 * @param m1  The matrix that is added
 * @param m2  The second matrix
 */
void matrix_add(struct matrix *m1, const struct matrix *m2);

/**
 * Delete the given matrix
 *
 * @param m  The matrix to delete
 */
void matrix_delete(struct matrix *m);
```


## Implémentation d'un tableau dynamique

Proposez une implémentation d'un tableau dynamique (*array*) à partir de l'interface suivante :


```c
// Types
// -----

typedef struct {
    int *values;     // Les valeurs dans le tableau
    int currentSize; // Le nombre d'elements dans le tableau
    int capacity;    // Capacite du tableau
} Array;

// Prototypes
// ----------

Array arrayCreate();
void arrayInsert(Array *a, int element);
void arrayRemove(Array *a, int i);
bool arrayHasElement(const Array *a, int element);
int arrayGet(const Array *a, int i);
void arrayDelete(Array *a);
```

Assurez-vous de gérer correctement la mémoire (pas de fuite)!