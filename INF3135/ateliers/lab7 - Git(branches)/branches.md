# Labo 7: Les branches sous Git

## 1 - Configuration et duplication (10 minutes)

* Récupérez le fichier [`gitconfig`](../config/gitconfig) disponible dans ce
  dépôt
* Adaptez votre propre fichier `~/.gitconfig` en y insérant les lignes
  manquantes
* En particulier, les synonymes `co`, `br` et `gr` seront utilisés
  régulièrement dans le laboratoire
* De plus, il est pratique d'utiliser Vim comme éditeur de texte pour les
  rebasements interactifs
* Puis, consultez le dépôt public
  [connect4](https://gitlab.info.uqam.ca/inf3135-sdo/connect4)
* Faites-en une copie (*fork*) à l'aide de l'interface web de GitLab
* Et clonez le projet sur votre machine locale

## 2 - Historique du projet (20 minutes)

Étudiez l'historique du projet, notamment à l'aide de

* `git gr`: pour visualiser le graphe et les branches
* `git br -a`: pour afficher la liste des branches locales et distantes
* `git show SHA`: pour visualiser les modifications apportées dans un *commit*
  dont l'identifiant est `SHA`
* `git co`: pour changer l'état d'une branche à l'autre

En vous fiant aux messages de *commit* seulement et aux noms des branches,
identifiez celui qui ne se trouve pas sur la bonne branche.

*Remarque*:

* Par défaut, après avoir cloné un dépôt, seule la branche local `master` est
  définie
* Mais si vous faites, par exemple, `git co readme`, alors Git va
  automatiquement créer une branche locale `readme` qui réfère au même *commit*
  que la branche `origin/readme`

## 3 - Intégration par sélection (`cherry-pick`) (15 minutes)

* Placez-vous sur la branche `master` avec `git co`
* Visualisez le contenu du *commit* de la branche `readme` avec `git show`
* Puis, intégrez ce *commit* sur `master` à l'aide de la commande `git
  cherry-pick`
* Confirmez que cela a fonctionné avec `git gr`
* Supprimez la branche locale `readme`, si elle existe, avec `git br -D`
* Supprimez la branche distante `origin/readme` à l'aide de l'interface web de
  GitLab
* Refaites maintenant `git gr`. Vous constaterez que `origin/readme` apparaît
  toujours. Pour la faire disparaître, il suffit de lancer la commande `git
  fetch --prune`.

## 4 - Intégration par fusion (`merge`) (15 minutes)

* Placez-vous à nouveau sur la branche `master`
* Intégrez la branche `make-clean` à l'aide de la commande `git merge
  make-clean`. Vous pouvez laisser le message par défaut proposé par Git.
* Visualisez à nouveau l'historique avec `git gr`. Vous remarquerez que vous
  avez un premier *commit* ayant deux parents. Les *commits* ayant deux parents
  sont toujours le résultat d'une fusion de deux branches divergentes.
* Supprimez la branche `make-clean`, si elle existe, avec `git br -d` (notez
  que `-d` suffit ici, car la branche est fusionnée, on n'a donc pas besoin de
  l'option `-D`)
* Vous pouvez aussi supprimer la branche distante `origin/make-clean` à l'aide
  de l'interface web

*Remarques*:

* La suppression de branche est optionnelle
* Mais elle permet de garder un historique plus propre
* En principe, on ne devrait supprimer que les branches intégrées (*merged*) ou
  les branches qu'on est sûr que personne d'autre n'utilise

## 5 - Rebasement simple (10 minutes)

* Placez-vous sur la branche `docstrings`
* Rebasez-la sur `master` à l'aide de la commande `git rebase master`
* Vérifiez le résultat avec `git gr`
* Vous remarquerez que vous avez non seulement récupéré les modifications
  spécifiques aux *docstrings*, mais également celle de la branche `play`.
* Vérifiez que le programme compile bien et que son comportement est correct.
* Puis mettez à jour la branche `master` pour qu'elle soit synchronisée avec
  `docstrings` à l'aide de la commande `git co -B master`.
* Supprimez les branches `docstrings` et `origin/docstrings`. Nous n'en avons
  plus besoin, car elles ont été intégrées à `master`

## 6 - Rebasement interactif (30 minutes)

* Placez-vous sur la branche `tests`
* Puis lancez la commande `git rebase -i master` pour lancer un rebasement
  interactif
* Si vous avez bien configuré votre fichier `.gitconfig` comme demandé, un
  *dialogue* Vim devrait s'ouvrir, dans lequel on vous demande d'expliquer
  comment faire le rebasement. Il s'agit d'un fichier texte dans lequel il
  y a un *commit* par ligne, et où vous pouvez préciser l'action à appliquer
  pour chacun de ces *commits*. Ceux-ci peuvent être
  
    * réordonnés (en permutant les lignes)
    * jetés (équivalent à remplacer `pick` par `drop`)
    * réécrits (en remplaçant `pick` par `reword`)
    * combinés (en remplaçant `pick` par `squash`)
    * modifiés (en remplaçant `pick` par `edit`), etc.

* En tout temps, vous pouvez annuler un rebasement interactif en tapant `git
  rebase --abort`
* Remplacez le troisième `pick` par `squash`
* Git tentera d'appliquer d'abord le premier *commit*, mais devra arrêter, car
  il y a un conflit. Ouvrez le fichier `connect4.c` et réglez le conflit. Il
  s'agit de repérer *toutes* les zones avec `<<<<<<<<`, `||||||||`, `========`
  et `>>>>>>>>` et de supprimer toutes les lignes sauf celles que vous
  souhaitez conserver. Dans l'exemple ici, il n'y aura qu'une seule zone, près
  de la fonction `print_board`. N'oubliez pas, lors de la résolution du
  conflit, de mettre à jour la *docstring* avec le paramètre `prefix` qui a été
  ajouté!
* Lorsque vous avez terminé, faites `git add connect4.c` pour confirmer que
  vous avez terminé de résoudre le conflit sur ce fichier, puis `git rebase
  --continue` pour poursuivre le rebasement.
* Git tentera ensuite d'appliquer le deuxième *commit* et arrêtera aussi en
  raison d'un autre conflit, cette fois-ci dans le fichier `Makefile`. Résolvez
  le conflit de la même façon que vous avez fait à l'étape précédente. Quand
  vous avez terminé, n'oubliez pas de faire `git add Makefile` et `git rebase
  --continue`.
* Git tentera d'appliquer le troisième *commit*, qui déclenchera
  également un conflit. Résolvez-le, comme à l'étape précédente, puis faites
  `git add Makefile` et `git rebase --continue`.
* Finalement, Git ouvre Vim une dernière fois pour vous demander de rédiger un
  message de *commit* qui combine les deux *commits* précédents. Écrivez un
  message qui résume bien les deux *commits*, puis quittez en sauvegardant.
* Confirmez que tout fonctionne bien en lançant `make` et `make test`!

